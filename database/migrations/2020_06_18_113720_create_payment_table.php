<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contract_number');
            $table->string('payment_name');
            $table->datetime('payment_date');
            $table->string('payment_type');
            $table->string('payment_customer_name')->nullable();
            $table->string('payment_period');
            $table->bigInteger('payment_price');
            $table->bigInteger('contract_id')->unsigned()->nullable();
            $table->foreign('contract_id')
                ->references('id')
                ->on('contracts')
                ->onDelete('CASCADE');
            $table->string('payment_hash')->nullable();
            $table->string('payment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
