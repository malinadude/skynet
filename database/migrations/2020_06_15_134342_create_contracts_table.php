<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contract_number');
            $table->datetime('contract_date')->nullable();
            $table->datetime('contract_renewal_date')->nullable();
            $table->boolean('contract_status')->nullable()->default(1);
            $table->bigInteger('contract_phone')->nullable();
            $table->float('contract_tariff')->nullable()->default(870);
            $table->string('contract_operator')->nullable();
            $table->string('contract_debt')->default(0);
            $table->string('contract_period')->nullable();
            $table->float('contract_speed')->nullable();
            $table->string('contract_type')->default('LTE');
            $table->string('contract_customer_surname')->nullable();
            $table->string('contract_customer_name')->nullable();
            $table->string('contract_patronymic')->nullable();
            $table->string('contract_customer_phone')->nullable();
            $table->string('contract_customer_second_phone')->nullable();
            $table->string('contract_customer_email')->nullable();
//            $table->string('contract_customer_region')->nullable();
            $table->string('contract_customer_full_address')->nullable();
//            $table->string('contract_customer_area')->nullable();
//            $table->string('contract_customer_locality')->nullable();
//            $table->string('contract_customer_street')->nullable();
//            $table->string('contract_customer_house_number')->nullable();
//            $table->string('contract_customer_type_of_room')->nullable();
            $table->datetime('contract_customer_birthday')->nullable();
            $table->string('contract_customer_passport_series')->nullable();
            $table->string('contract_customer_passport_number')->nullable();
            $table->string('contract_customer_passport_code')->nullable();
            $table->datetime('contract_customer_passport_date_of_issue')->nullable();
            $table->string('contract_customer_passport_issued_by')->nullable();
            $table->string('contract_customer_passport_registration_address')->nullable();
            $table->string('contract_comment_one')->nullable();
            $table->string('contract_comment_two')->nullable();
            $table->string('contract_price')->nullable();
            $table->string('contract_installer')->nullable();
            $table->string('contract_serial')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
