<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Contract;

class WriteOffContracts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'writeOffContracts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $dayOfWriteOff = '1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function parseContractPeriod($period) {
        $month = substr($period, 0, 2);
        $year = '20' . substr($period, 2, 2);
        $date = $year . '-' . $month . '-' . $this->dayOfWriteOff;

        return date("d.m.Y", strtotime($date));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentPeriod = date("my");

        $periodPrev = Date("my", strtotime("20" . substr($currentPeriod, 2, 2) . "-" . substr($currentPeriod, 0, 2) . "-01" . " -1 month"));

//        $contracts = Contract::where([
//                ['contract_status', '=', 1],
//                ['contract_period', $periodPrev]
//            ])
//            ->get();
//
//        var_dump($contracts);

        $contracts = Contract::where([
                ['contract_status', '=', 1],
                ['contract_period', '=', $periodPrev]
            ])
            ->orWhere([
                ['contract_status', '=', 2],
                ['contract_period', '=', $periodPrev]
            ])
            ->whereNotNull('contract_period')
            ->get();

//        var_dump($contracts);

        foreach ($contracts as $contractKey => $contract) {
            $period = $this->parseContractPeriod($contract->contract_period);

//            var_dump(date("d"), $this->dayOfWriteOff, date("d") === $this->dayOfWriteOff && $this->parseContractPeriod(Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-01" . " +1 month"))) === date("d.m.Y"), $this->parseContractPeriod(Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-01" . " +1 month"))), date("d.m.Y"), $this->parseContractPeriod(Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-01" . " +1 month"))) === date("d.m.Y"));

//            var_dump(
//                date("d") === $this->dayOfWriteOff && $this->parseContractPeriod(Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-01" . " +1 month"))) === date("d.m.Y")
//            );

            if (date("d") === $this->dayOfWriteOff && $this->parseContractPeriod(Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-01" . " +1 month"))) === date("d.m.Y")) {
                if ($contract->contract_status === 1) {
                    $contract->createPayment(
                        $contract->contract_number,
                        'Оплата услуг',
                        date("Y-m-d H:i:s"),
                        'Автосписание со счета',
                        $contract->contract_tariff,
                        null,
                        $contract->contract_period,
                        'Автосписание'
                    );

                    $periodUpdated = Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-01" . " +1 month"));

//                    $periodYear = substr($period, 2, 2);
//
//                    if (substr($periodMonth, 0, 2) === '01' || substr($periodMonth, 0, 2) === '02') {
//                        $periodYear = Date("y", strtotime("20" . substr($period, 2, 2) . "-" . substr($period, 0, 2) . "-01" . " +1 year"));
//                    }
                } elseif ($contract->contract_status === 2) {
                    $contract->createPayment(
                        $contract->contract_number,
                        'Оплата услуг',
                        date("Y-m-d H:i:s"),
                        'Автосписание со счета',
                        $contract->contract_tariff * 2,
                        null,
                        $contract->contract_period,
                        'Автосписание'
                    );

                    $periodUpdated = Date("my", strtotime("20" . substr($contract->contract_period, 2, 2) . "-" . substr($contract->contract_period, 0, 2) . "-02" . " +2 month"));

//                    $periodYear = substr($period, 2, 2);
//
//                    if (substr($periodMonth, 0, 2) === '01' || substr($periodMonth, 0, 2) === '02') {
//                        $periodYear = Date("y", strtotime("20" . substr($period, 2, 2) . "-" . substr($period, 0, 2) . "-01" . " +1 year"));
//                    }
                }

                $contract->contract_period = $periodUpdated;
                $contract->save();
            }
        }
    }
}
