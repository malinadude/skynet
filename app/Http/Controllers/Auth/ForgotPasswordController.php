<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use http\Env\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Change the specified resource from storage.
     *
     * @param HttpRequest $request
     * @return mixed
     */
    public function forgotPassword(HttpRequest $request) {
        if ($request->phone) {
            Mail::send('layouts.send-phone-mail', array('phone' => $request->phone), function($message)
            {
                $message->from('postmaster@sandboxc0919c3aab4447ac94d7a14f2fe009fe.mailgun.org', 'lk.skynet');
                $message->to('stas_med4848@mail.ru')->subject('Восстановление пароля');
            });
        }
    }
}
