<?php

namespace App\Http\Controllers;

use App\Payment;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Contract;
use App\ContractSettings;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Str;

class ContractController extends Controller
{
    //@TODO malinadude переделать на фильтрацию ларавел
    public function contract_validate($form)
    {
        $errors = [];

        if (Contract::where('contract_number', $form->post('contract_number'))->exists()) {
            array_push($errors, 'Существующий номер договора (Договор)');
        }

        if (preg_match("/^\s*((?:19|20)\d{2})\-(1[012]|0?[1-9])\-(3[01]|[12][0-9]|0?[1-9]) (\d{2})\:(\d{2})\:(\d{2})\s*$/", $form->post('contract_date')) === 0) {
            array_push($errors, 'Несоответствующий формат даты (Дата)');
        }

        if (preg_match("/^\d{10}$/", $form->post('contract_phone')) === 0) {
            array_push($errors, 'Неверное количество символов (Номер)');
        }

//        if (iconv_strlen(strval($form->post('contract_renewal_date'))) < 2) {
//            array_push($errors, 'Неверное количество символов (Дата списания)');
//        }

        if (preg_match("/^(1[012]|0?[1-9])(\d{2})\s*$/", $form->post('contract_period')) === 0) {
            array_push($errors, 'Несоответствующий формат даты (Период)');
        }

        if ($form->post('user_password') && iconv_strlen(strval($form->post('user_password'))) < 10) {
            array_push($errors, 'Неверное количество символов (Пароль)');
        }

        if (preg_match("/[\d]+/", $form->post('contract_customer_surname')) === 1) {
            array_push($errors, 'Строка не может содержать цифры (Фамилия)');
        }

        if (preg_match("/[\d]+/", $form->post('contract_customer_name')) === 1) {
            array_push($errors, 'Строка не может содержать цифры (Имя)');
        }

        if (preg_match("/[\d]+/", $form->post('contract_patronymic')) === 1) {
            array_push($errors, 'Строка не может содержать цифры (Отчество)');
        }

        if (preg_match("/^\\+(([0-9])\d{10})*$/", $form->post('contract_customer_phone')) === 0) {
            array_push($errors, 'Несоответствующий формат моб. телефона (Телефон)');
        }

        if ($form->post('contract_customer_second_phone') && preg_match("/^\\+(([0-9])\d{10})*$/", $form->post('contract_customer_second_phone')) === 0) {
            array_push($errors, 'Несоответствующий формат моб. телефона (Доп. телефон)');
        }

        if ($form->post('contract_customer_email') && preg_match("/^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $form->post('contract_customer_email')) === 0) {
            array_push($errors, 'Несоответствующий формат E-mail (E-mail)');
        }

        if (preg_match("/^\s*((?:19|20)\d{2})\-(1[012]|0?[1-9])\-(3[01]|[12][0-9]|0?[1-9]) (\d{2})\:(\d{2})\:(\d{2})\s*$/", $form->post('contract_customer_birthday')) === 0) {
            array_push($errors, 'Несоответствующий формат даты (Дата рождения)');
        }

        if (preg_match("/^\d{4}$/", $form->post('contract_customer_passport_series')) === 0) {
            array_push($errors, 'Неверное количество символов (Серия паспорта)');
        }

        if (preg_match("/^\d{6}$/", $form->post('contract_customer_passport_number')) === 0) {
            array_push($errors, 'Неверное количество символов (Номер паспорта)');
        }

        if (preg_match("/^([0-9]\d{2})\-([0-9]\d{2})\s*$/", $form->post('contract_customer_passport_code')) === 0) {
            array_push($errors, 'Несоответствующий формат (Код паспорта)');
        }

        if (preg_match("/^\s*((?:19|20)\d{2})\-(1[012]|0?[1-9])\-(3[01]|[12][0-9]|0?[1-9]) (\d{2})\:(\d{2})\:(\d{2})\s*$/", $form->post('contract_customer_passport_date_of_issue')) === 0) {
            array_push($errors, 'Несоответствующий формат даты (Дата выдачи)');
        }

        return $errors;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $errors = $this->contract_validate($request);

        if (count($errors) === 0) {
            $contract = Contract::create($request->all());
            $data = [
                'type' => 'contract',
                'items' => $contract
            ];

            User::create([
                'login' => $contract->contract_number,
                'phone' => $contract->contract_customer_phone,
                'password' => Hash::make($request->post('user_password')),
                'type' => User::DEFAULT_TYPE,
                'contract_id' => $contract->id,
                'email' => $contract->contract_customer_email,
            ]);

            return response()->json($data);
        } else {
            $data = [
                'type' => 'errors',
                'items' => $errors
            ];

            return response()->json($data);
        }
    }

    public function generateUsers()
    {
        $contracts = Contract::orderBy('contract_number', 'desc')->get();

        foreach ($contracts as $contract) {
            User::create([
                'login' => $contract->contract_number,
                'phone' => $contract->contract_customer_phone,
                'password' => Hash::make('1234567890'),
                'type' => User::DEFAULT_TYPE,
                'contract_id' => $contract->id,
                'email' => $contract->contract_customer_email,
                'api_token' => Str::random(80),
            ]);
        }
    }

    /**
     * Returns all categories existing in our database.
     *
     * @return JsonResponse
     */
    public function get()
    {
        $contracts = Contract::orderBy('contract_number', 'desc')
            ->selectRaw("REPLACE(contract_debt, ',', '.') + 0 as 'contract_debt'")
            ->addSelect('contract_number')
            ->addSelect('contract_date')
            ->addSelect('contract_renewal_date')
            ->addSelect('contract_status')
            ->addSelect('contract_phone')
            ->addSelect('contract_tariff')
            ->addSelect('contract_operator')
            ->addSelect('contract_period')
            ->addSelect('contract_speed')
            ->addSelect('contract_type')
            ->addSelect('contract_customer_surname')
            ->addSelect('contract_customer_name')
            ->addSelect('contract_patronymic')
            ->addSelect('contract_customer_phone')
            ->addSelect('contract_customer_second_phone')
            ->addSelect('contract_customer_email')
            ->addSelect('contract_customer_full_address')
            ->addSelect('contract_customer_birthday')
            ->addSelect('contract_customer_passport_series')
            ->addSelect('contract_customer_passport_number')
            ->addSelect('contract_customer_passport_code')
            ->addSelect('contract_customer_passport_date_of_issue')
            ->addSelect('contract_customer_passport_issued_by')
            ->addSelect('contract_customer_passport_registration_address')
            ->addSelect('contract_comment_one')
            ->addSelect('contract_comment_two')
            ->addSelect('contract_price')
            ->addSelect('contract_installer')
            ->addSelect('contract_district')
            ->get();

        return response()->json($contracts);
    }

    /**
     * Return category by id existing in our database.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getById(int $id)
    {
        $contract = Contract::where('contract_number', $id)->get();

        return response()->json($contract);
    }

    public function getUserContract()
    {
        $user = Auth::guard('api')->user();

        $contract = Contract::where('id', $user->contract_id)->first();

        return response()->json($contract);
    }

    public function getContractSettings()
    {
        return ContractSettings::get();
    }

    /**
     * Change the specified resource from storage.
     *
     * @param Request $request
     * @return mixed
     */
    public function change(Request $request)
    {
        $errors = $this->contract_validate($request);

        if (array_search('Существующий номер договора (Договор)', $errors) !== false && $request->post('current_id') === $request->post('contract_number')) {
            unset($errors[array_search('Существующий номер договора (Договор)', $errors)]);
        }

        if (count($errors) === 0) {
            $contract = Contract::where('contract_number', $request->post('current_id'))->first();
            $payments = Payment::where('contract_number', $request->post('current_id'))->get();

            foreach ($payments as $key => $payment) {
                $payment->contract_id = $contract->getContractId();
                $payment->contract_number = $request->post('contract_number');
                $payment->save();
            }

            $contract->update($request->all());

            $contract->save();

            if ($request->post('user_password')) {
                $user = $contract->user;

                $user->setUserPassword($user, $request->post('user_password'));
            }

            return true;
        } else {
            $data = [
                'type' => 'errors',
                'items' => $errors
            ];

            return response()->json($data);
        }
    }

    public function getBalance()
    {
        $contracts_full = '';
        $currentMonth = date("m");
        $currentYear = date("y");
        $currentPeriod = $currentMonth . $currentYear;
//        $contracts = Contract::where('contract_period', $currentPeriod)
//            ->whereIn('contract_status', [1, 2, 3, 9])
//            ->get();

        $contractsWhatsApp = Contract::where([
                ['contract_status', 1]
            ])
            ->get()
            ->toArray();

        $contractsMail = Contract::where([
                ['contract_status', 2]
            ])
            ->get()
            ->toArray();

        $contractsEmail = Contract::where([
                ['contract_status', 3]
            ])
            ->get()
            ->toArray();

        $contractsDisabled = Contract::where([
                ['contract_status', 0]
            ])
            ->get()
            ->toArray();

        $contractsStopped = Contract::where([
                ['contract_status', 9]
            ])
            ->get()
            ->toArray();

        $contracts = array_merge(
            $contractsWhatsApp,
            $contractsMail,
            $contractsEmail,
            $contractsDisabled,
            $contractsStopped
        );

        foreach ($contracts as $key => $contract) {
            $contract_debt = (float) str_replace(',', '.', $contract['contract_debt']);

            if ($contract['contract_status'] === 0) {
                if ($contract_debt >= 0) {
                    unset($contracts[$key]);
                    continue;
                } else {
                    $sum = $contract_debt;
                }
            }

            if ($contract['contract_status'] === 9) {
                if ($contract_debt >= 0) {
                    $sum = 0;
                } else {
                    $sum = $contract_debt;
                }
            }

            if ($contract['contract_status'] === 0 || $contract['contract_status'] === 9) {
                $contract['contract_tariff'] = 0;
                $contract['contract_period'] = $currentPeriod;
            }

            if ($contract['contract_status'] === 2) {
                $periodUpdated = Date("my", strtotime("20" . substr($contract['contract_period'], 2, 2) . "-" . substr($contract['contract_period'], 0, 2) . "-02" . " +2 month"));

                $contractSum = $contract['contract_tariff'] * 2;
            } else {
                $periodUpdated = Date("my", strtotime("20" . substr($contract['contract_period'], 2, 2) . "-" . substr($contract['contract_period'], 0, 2) . "-01" . " +1 month"));

                $contractSum = $contract['contract_tariff'];
            }

            if ($contract['contract_status'] !== 0) {
                $sum = $contract_debt;

                if ($sum > 0) {
                    while ($sum > 0) {
                        $sum = $sum - $contract['contract_tariff'];
                    }
                }

//                if ($contract['contract_number'] === '61770025') {
//
//                } else {
////                    if ($contract_debt > 0 && $contract_debt < $contractSum) {
////                        $sum = abs($contract_debt - $contractSum);
////                    }
//
////                    if ($contract_debt > 0 && $contract_debt >= $contractSum) {
////                        $sum = 0;
////                    }
//
////                    if ($contract_debt < 0) {
////                        $sum = abs($contract_debt) + $contractSum;
////                    }
//
////                    if ($contract_debt == 0) {
////                        $sum = $contractSum;
////                    }
//                }
            }

            $contract_balance = $contract['contract_number'] . '; ';
            $contract_balance .= $contract['contract_customer_surname'] . ' ' . $contract['contract_customer_name'] . ' ' . $contract['contract_patronymic'] . '; ';
            $contract_balance .= $contract['contract_customer_full_address'] . '; ';
            $contract_balance .= $periodUpdated . '; ';
            $contract_balance .= number_format(abs($sum), 2, ',','') . ' ';
            $contracts_full .= $contract_balance . "\n";
        }

        return response($contracts_full);
    }

    /**
     * Account balance replenishment.
     *
     * @param Request $request
     */
    public function replenish(Request $request)
    {
        $contract = Contract::where('contract_number', $request->post('contract_number'))->first();
        $contract->createPayment(
            $request->post('contract_number'),
            $request->post('payment_name'),
            $request->post('payment_date'),
            $request->post('payment_type'),
            $request->post('payment_price'),
            $request->post('payment_customer_name'),
            $request->post('payment_period'),
            $request->post('payment_status')
        );
    }

    public function importBalances(Request $request)
    {
        if ($request->input('submit') != null ) {
            $file = $request->file('file');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv");

            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){

                // Check file size
                if($fileSize <= $maxFileSize){

                    // File upload location
                    $location = 'uploads';

                    // Upload file
                    $file->move($location,$filename);

                    // Import CSV to Database
                    $filepath = public_path($location."/".$filename);

                    // Reading file
                    $file = fopen($filepath,"r");

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ";")) !== FALSE) {
                        $num = count($filedata );

                        // Skip first row (Remove below comment if you want to skip the first row)
                        /*if($i == 0){
                           $i++;
                           continue;
                        }*/
                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata [$c];
                        }
                        $i++;
                    }
                    fclose($file);

                    // Insert to MySQL database
                    foreach($importData_arr as $importData){
                        $contract = Contract::where('contract_number', preg_replace("/\s+/", "", $importData[0]))->first();

                        if ($contract) {
                            $contract->contract_period = preg_replace("/\s+/", "", $importData[3]);
                            $contract->contract_debt = (float) preg_replace("/\s+/", "", $importData[4]) != 0 ? (float) preg_replace("/\s+/", "", $importData[4]) : (float) preg_replace("/\s+/", "", $importData[4]);
                            $contract->save();
                        }

//                        $insertData = array(
//                            "username"=>$importData[1],
//                            "name"=>$importData[2],
//                            "gender"=>$importData[3],
//                            "email"=>$importData[4]);
//                        Page::insertData($insertData);
                    }
                }
            }
        }
    }

    /**
     * Account balance replenishment.
     *
     * @param Request $request
     * @return int[]
     */
    public function importPayments(Request $request)
    {
        $payments = json_decode($request->post('payments'));
        $contracted_payments = 0;
        $non_contractual_payments = 0;
        $existing_payments = 0;

        foreach ($payments as $key => $payment) {
            if ($payment != '' && is_object($payment)) {
                $contract = Contract::where('contract_number', $payment->contract_number)->first();
                $existing_payment = Payment::where([
                        ['contract_number', $payment->contract_number],
                        ['payment_name', $payment->payment_name],
                        ['payment_date', $payment->payment_date],
                        ['payment_type', $payment->payment_type],
                        ['payment_price', $payment->payment_price],
                        ['payment_customer_name', $payment->payment_customer_name],
                        ['payment_period', $payment->payment_period],
                    ])
                    ->exists();

                if (!$existing_payment) {
                    if ($contract) {
                        $contract->createPayment(
                            $payment->contract_number,
                            $payment->payment_name,
                            $payment->payment_date,
                            $payment->payment_type,
                            $payment->payment_price,
                            $payment->payment_customer_name,
                            $payment->payment_period,
                            'Оплачен'
                        );

                        $contracted_payments++;
                    } else {
                        $payment = new Payment([
                            'contract_id' => null,
                            'contract_number' => $payment->contract_number,
                            'payment_name' => $payment->payment_name,
                            'payment_date' => $payment->payment_date,
                            'payment_type' => $payment->payment_type,
                            'payment_price' => $payment->payment_price,
                            'payment_customer_name' => $payment->payment_customer_name,
                            'payment_period' => $payment->payment_period,
                            'payment_status' => 'Оплачен',
                        ]);

                        $payment->save();

                        $non_contractual_payments++;
                    }
                } else {
                    $existing_payments++;
                }
            }
        }

        return [
            'contracted_payments' => $contracted_payments,
            'non_contractual_payments' => $non_contractual_payments,
            'payments_total' => $contracted_payments + $non_contractual_payments,
            'existing_payments' => $existing_payments
        ];
    }

    /**
     * Return payments by id existing in our database.
     *
     * @return JsonResponse
     */
    public function paymentsUserGet()
    {
        $user = Auth::guard('api')->user();

        $payments = Payment::where([
                ['contract_id', $user->contract_id],
                ['payment_status', '!=', 'Заказ зарегистрирован, но не оплачен'],
                ['payment_status', '!=', 'Предавторизованная сумма захолдирована (для двухстадийных платежей)'],
                ['payment_status', '!=', 'Авторизация отменена'],
                ['payment_status', '!=', 'По транзакции была проведена операция возврата'],
                ['payment_status', '!=', 'Инициирована авторизация через ACS банка-эмитента'],
                ['payment_status', '!=', 'Авторизация отклонена'],
                ['payment_status', '!=', 'Не оплачен']
            ])
            ->orderBy('payment_date', 'desc')
            ->get();

        return response()->json($payments);
    }

    /**
     * Return payments by id existing in our database.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function paymentsGet(int $id)
    {
        $contract = Contract::where('contract_number', $id)->first();
        $payments = Payment::where([
            ['contract_id', $contract->getContractId()],
            ['payment_status', '!=', 'Заказ зарегистрирован, но не оплачен'],
            ['payment_status', '!=', 'Предавторизованная сумма захолдирована (для двухстадийных платежей)'],
            ['payment_status', '!=', 'Авторизация отменена'],
            ['payment_status', '!=', 'По транзакции была проведена операция возврата'],
            ['payment_status', '!=', 'Инициирована авторизация через ACS банка-эмитента'],
            ['payment_status', '!=', 'Авторизация отклонена'],
            ['payment_status', '!=', 'Не оплачен']
        ])
            ->orderBy('payment_date', 'desc')
            ->get();

        return response()->json($payments);
    }

    /**
     * Return payments by id existing in our database.
     *
     * @return JsonResponse
     */
    public function paymentsAllGet()
    {
        $payments = Payment::where([
            ['payment_status', '!=', 'Заказ зарегистрирован, но не оплачен'],
            ['payment_status', '!=', 'Предавторизованная сумма захолдирована (для двухстадийных платежей)'],
            ['payment_status', '!=', 'Авторизация отменена'],
            ['payment_status', '!=', 'По транзакции была проведена операция возврата'],
            ['payment_status', '!=', 'Инициирована авторизация через ACS банка-эмитента'],
            ['payment_status', '!=', 'Авторизация отклонена'],
            ['payment_status', '!=', 'Не оплачен']
        ])
            ->orderBy('payment_date', 'desc')
            ->get();

        return response()->json($payments);
    }

    /**
     * Change the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function paymentChange(Request $request)
    {
        $payment = Payment::where('id', $request->post('id'))->first();
        $payment->update($request->all());
        $payment->save();

        $contract = Contract::where('id', $payment->contract_id)->first();
        $contract->paymentRecount($payment);

        return response()->json("ok");
    }

    /**
     * Returns all categories existing in our database.
     *
     * @return mixed
     */
    public function receiptsGet()
    {
        $currentMonth = date("m");
        $currentYear = date("y");
        $currentPeriod = $currentMonth . $currentYear;

        $contractsWhatsApp = Contract::where([
                ['contract_status', 1],
                ['contract_period', $currentPeriod]
            ])
            ->get();

        $contractsMail = Contract::where([
                ['contract_status', 2],
                ['contract_period', $currentPeriod]
            ])
            ->get();

        $contractsEmail = Contract::where([
                ['contract_status', 3],
                ['contract_period', $currentPeriod]
            ])
            ->get();

        $contractsDisabled = Contract::where([
                ['contract_status', 0]
            ])
            ->get();

        $contractsStopped = Contract::where([
                ['contract_status', 9]
            ])
            ->get();

        $contracts = [];

        if (count($contractsWhatsApp) > 0) {
            array_push($contracts, [
                'receipts_type' => 'whatsapp',
                'receipts_name' => "1 - What'sApp",
                'receipts_period' => $currentPeriod,
                'receipts_items' => $contractsWhatsApp->all(),
                'receipts_count' => count($contractsWhatsApp)
            ]);
        }

        if (count($contractsMail) > 0) {
            array_push($contracts, [
                'receipts_type' => 'mail',
                'receipts_name' => "2 - Почта",
                'receipts_period' => $currentPeriod,
                'receipts_items' => $contractsMail->all(),
                'receipts_count' => count($contractsMail)
            ]);
        }

        if (count($contractsEmail) > 0) {
            array_push($contracts, [
                'receipts_type' => 'email',
                'receipts_name' => "3 - Email",
                'receipts_period' => $currentPeriod,
                'receipts_items' => $contractsEmail->all(),
            ]);
        }

        if (count($contractsDisabled) > 0) {
            array_push($contracts, [
                'receipts_type' => 'disabled',
                'receipts_name' => "0 - Отключен",
                'receipts_period' => $currentPeriod,
                'receipts_items' => $contractsDisabled->all(),
            ]);
        }

        if (count($contractsStopped) > 0) {
            array_push($contracts, [
                'receipts_type' => 'stopped',
                'receipts_name' => "9 - Остановлен",
                'receipts_period' => $currentPeriod,
                'receipts_items' => $contractsStopped->all(),
            ]);
        }

        foreach ($contracts as $key => $contractGroup) {
            $contractsSum = 0;
            $contractsDebt = 0;

            foreach ($contractGroup['receipts_items'] as $itemKey => $contract) {
                $contract_debt = (float) str_replace(',', '.', $contract->contract_debt);

                if ($contractGroup['receipts_type'] === 'disabled' || $contractGroup['receipts_type'] === 'stopped') {
                    if ($contract_debt >= 0) {
                        unset($contractGroup['receipts_items'][$itemKey]);
                        continue;
                    } else {
                        $contract->contract_tariff = 0;
                        $contract->contract_period = $currentPeriod;
                    }
                } else {
                    if ($contractGroup['receipts_type'] === 'mail') {
                        $contractSum = $contract->contract_tariff * 2;
                    } else {
                        $contractSum = $contract->contract_tariff;
                    }

                    if ($contract_debt > 0 && $contract_debt >= $contractSum) {
//                        $contractSum = 0;

                        unset($contractGroup['receipts_items'][$itemKey]);
                        continue;
                    }

                    if ($contract_debt > 0 && $contract_debt < $contractSum) {
                        $contractSum = $contractSum - abs($contract_debt);
                    }

                    $contractsSum += abs($contractSum);
                }

                if ($contract_debt < 0) {
                    $contractsDebt += $contract_debt;
                }
            }

            $contractGroup['receipts_items'] = array_values($contractGroup['receipts_items']);

            if (count($contractGroup['receipts_items']) <= 0) {
                unset($contracts[$key]);
                continue;
            }

            $contracts[$key]['receipts_sum'] = $contractsSum;
            $contracts[$key]['receipts_debt'] = $contractsDebt;
            $contracts[$key]['receipts_items'] = $contractGroup['receipts_items'];
            $contracts[$key]['receipts_count'] = count($contractGroup['receipts_items']);
        }

        $contracts = array_values($contracts);

        return response()->json($contracts);
    }
}
