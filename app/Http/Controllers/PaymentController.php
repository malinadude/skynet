<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\ContractController;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Voronkovich\SberbankAcquiring\OrderStatus;
use DateTime;


class PaymentController extends Controller
{
    private $client;
    private $payment_number;
    private $payment_hash;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client(
            [
                'userName' => 'i-skynet-api',
                'password' => 'i-skynet',
                'apiUri' => 'https://3dsec.sberbank.ru'
            ]
        );
    }

    /**
     * Generating payment url.
     *
     * @param Request $request
     * @return string
     */
    public function generate_url(Request $request)
    {

        $contract = Contract::where('contract_number', $request->post('contract_number'))->first();
        $this->payment_number = $contract->createPayment(
            $request->post('contract_number'),
            $request->post('payment_name'),
            $request->post('payment_date'),
            $request->post('payment_type'),
            $request->post('payment_price'),
            $request->post('payment_customer_name'),
            $request->post('payment_period'),
            'Не оплачен'
        );
        $payment = Payment::where('id', $this->payment_number)->first();

        $payment_price        = (int) $request->post('payment_price') * 100;
        $return_url           = 'http://skynet.test/api/payment/get-status';


        $params['currency'] = Currency::RUB;
        $params['failUrl']  = 'http://skynet.test/api/payment/get-status';

        $date = new DateTime();
        $date = $date->getTimestamp();

//        $result = $this->client->registerOrder($this->payment_number, $payment_price, $return_url, $params);
        $result = $this->client->registerOrder($date, $payment_price, $return_url, $params);

        $payment->payment_hash = $result['orderId'];
        $payment->save();

        return $result['formUrl'];
    }

    public function payment_status(Request $request)
    {
        $payment = Payment::where('payment_hash', $request->input('orderId'))->first();

        if ($payment->payment_status != 'Платеж завершен') {
            $order_status_codes = [
                0 => 'Заказ зарегистрирован, но не оплачен',
                1 => 'Предавторизованная сумма захолдирована (для двухстадийных платежей)',
                2 => 'Проведена полная авторизация суммы заказа',
                3 => 'Авторизация отменена',
                4 => 'По транзакции была проведена операция возврата',
                5 => 'Инициирована авторизация через ACS банка-эмитента',
                6 => 'Авторизация отклонена',
            ];

            $result = $this->client->getOrderStatus($request->input('orderId'));
//        $result = $this->client->getOrderStatus('046c56aa-9df3-7cb4-bf01-46e75e387810');

            $payment->payment_status = $order_status_codes[$result['orderStatus']];

            $contract = Contract::where('contract_number', $payment->contract_number)->first();
            $contract->paymentRecount($payment);

            $payment->save();
        }
    }
}
