<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function redirect()
    {
        if (! auth()->check() ) {
            return redirect()->to( '/login' );
        }

        if (auth()->user()->role === 'admin' || auth()->user()->role === 'manager') {
            return redirect()->to( '/billing' );
        }

        if (auth()->user()->role === 'user') {
            return redirect()->to( '/account' );
        }
    }
}
