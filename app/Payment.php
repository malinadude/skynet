<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contract;

class Payment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'payment_hash',
        'contract_number',
        'payment_name',
        'payment_date',
        'payment_type',
        'payment_price',
        'contract_id',
        'payment_customer_name',
        'payment_period',
        'payment_status',
    ];

    public function contract()
    {
        return $this->hasOne('App\Contract');
    }
}
