<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;
use Ramsey\Uuid\Type\Integer;
use App\User;

class Contract extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'contract_number',
        'contract_date',
        'contract_renewal_date',
        'contract_status',
        'contract_phone',
        'contract_tariff',
        'contract_operator',
        'contract_debt',
        'contract_period',
        'contract_speed',
        'contract_type',
        'contract_password',
        'contract_customer_surname',
        'contract_customer_name',
        'contract_patronymic',
        'contract_customer_phone',
        'contract_customer_second_phone',
        'contract_customer_email',
        'contract_customer_full_address',
        'contract_district',
        'contract_customer_region',
        'contract_customer_area',
        'contract_customer_locality',
        'contract_customer_street',
        'contract_customer_house_number',
        'contract_customer_type_of_room',
        'contract_customer_birthday',
        'contract_customer_passport_series',
        'contract_customer_passport_number',
        'contract_customer_passport_code',
        'contract_customer_passport_date_of_issue',
        'contract_customer_passport_issued_by',
        'contract_customer_passport_registration_address',
        'contract_comment_one',
        'contract_comment_two',
        'contract_price',
        'contract_installer'
    ];

    function checkPaymentStatus($payment_status) {
        $validate_status = false;

        //@TODO malinadude переделать с магических строк на нормальные условия
        if (
            $payment_status != 'Заказ зарегистрирован, но не оплачен'
            && $payment_status != 'Предавторизованная сумма захолдирована (для двухстадийных платежей)'
            && $payment_status != 'Авторизация отменена'
            && $payment_status != 'По транзакции была проведена операция возврата'
            && $payment_status != 'Инициирована авторизация через ACS банка-эмитента'
            && $payment_status != 'Авторизация отклонена'
            && $payment_status != 'Не оплачен'
        ) {
            $validate_status = true;
        }

        return $validate_status;
    }

    public function createPayment(
        String $contract_number,
        String $payment_name,
        String $payment_date,
        String $payment_type,
        Float $payment_price,
        $payment_customer_name,
        String $payment_period,
        String $payment_status
    ) : Int
    {
        $payment = new Payment([
            'contract_id' => $this->getContractId(),
            'contract_number' => $contract_number,
            'payment_name' => $payment_name,
            'payment_date' => $payment_date,
            'payment_type' => $payment_type,
            'payment_price' => $payment_price,
            'payment_customer_name' => $payment_customer_name,
            'payment_period' => $payment_period,
            'payment_status' => $payment_status,
        ]);

        $this->paymentRecount($payment);

        return $payment->id;
    }

    public function paymentRecount($payment) {
        if ($payment->payment_name == 'Пополнение баланса' && $this->checkPaymentStatus($payment->payment_status)) {
            $payment->payment_status = 'Платеж завершен';
            $this->contract_debt = str_replace('.', ',', floatval(str_replace(',', '.', $this->contract_debt)) + $payment->payment_price);
        } else if ($payment->payment_name == 'Оплата услуг' && $this->checkPaymentStatus($payment->payment_status)) {
            $payment->payment_status = 'Платеж завершен';
            $this->contract_debt = str_replace('.', ',', floatval(str_replace(',', '.', $this->contract_debt)) - $payment->payment_price);
        }

        $payment->save();
        $this->save();
    }
    
    public function getContractId()
    {
        return $this->id;
    }

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
