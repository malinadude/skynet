<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Boolean;
use App\Contract;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ADMIN_TYPE = 'admin';
    const MANAGER_TYPE = 'manager';
    const DEFAULT_TYPE = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'contract_id',
        'role',
        'phone',
        'email',
        'password',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function contract()
    {
        return $this->hasOne(Contract::class);
    }

    public function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
        return $user->save();
    }

    public function getUser() {
        return $this;
    }

    /**
     * Check user is admin.
     *
     * @var array
     * @return bool
     */
    public function isAdmin(): Bool {
        return $this->role === self::ADMIN_TYPE;
    }

    /**
     * Check user is manager.
     *
     * @var array
     * @return bool
     */
    public function isManager(): Bool {
        return $this->role === self::ADMIN_TYPE || $this->role === self::MANAGER_TYPE;
    }

    /**
     * Check user contract number.
     *
     * @var array
     * @return bool
     */
    public function isUser(): Bool {
        return $this->login === self::ADMIN_TYPE || $this->role === self::MANAGER_TYPE;
    }
}
