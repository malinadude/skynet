<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>'auth:api'], function(){
    Route::post('account/contract', 'ContractController@getUserContract');
    Route::get('payment/get/user', 'ContractController@paymentsUserGet');
});

Route::group(['middleware' => ['auth:api', 'is_manager']], function() {
    //Billing
    Route::get('balance/export', 'ContractController@getBalance');

    //Contract
    Route::get('contract/get', 'ContractController@get');
    Route::post('contract/add', 'ContractController@store');
    Route::post('contract/change', 'ContractController@change');
    Route::get('payment/get', 'ContractController@paymentsAllGet');
    Route::post('payment/replenish', 'ContractController@replenish');
    Route::post('payment/import', 'ContractController@importPayments');
    Route::post('payment/change', 'ContractController@paymentChange');
    Route::get('contract/settings', 'ContractController@getContractSettings');
    Route::get('contract/id/{id}', 'ContractController@getById');

    //Payment
    Route::get('payment/get/{id}', 'ContractController@paymentsGet');

    //Receipts
    Route::get('receipts/get', 'ContractController@receiptsGet');
});

Route::post('contract/import-balances', 'ContractController@importBalances');

Route::post('contract/generate-users', 'ContractController@generateUsers');
Route::post('forgot-password', 'Auth\ForgotPasswordController@forgotPassword');

//Sberbank Payment
Route::post('payment/generate', 'PaymentController@generate_url');
Route::get('payment/get-status', 'PaymentController@payment_status');