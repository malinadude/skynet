<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@redirect')->name('/');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/billing', function () {
    return view('pages/billing');
})->middleware('is_manager');

Route::get('/payments', function () {
    return view('pages/payments');
})->middleware('is_manager');

Route::get('/receipts', function () {
    return view('pages/receipts');
})->middleware('is_manager');

Route::get('/contract/add', function () {
    return view('pages/contract');
})->middleware('is_manager');

Route::get('/contract/id/{contractId}', function ($contractId) {
    $contract = \App\Contract::where('contract_number', $contractId)->exists();

    if ($contract) {
        return view('pages/contract');
    } else {
        abort('404');
    }
})->middleware('is_manager');

Route::get('/payment/status', function () {
    return view('pages/payment/status');
});

Route::get('/account', function () {
    return view('pages/account');
})->middleware('auth');

//->middleware('is_admin');
