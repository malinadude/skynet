import Vue from 'vue';
import Router from 'vue-router';
import Billing from '../views/Billing';
import Contract from '../views/Contract';
import Payments from '../views/Payments';
import Receipts from '../views/Receipts';
import Account from '../views/Account';
import ForgotPassword from '../views/ForgotPassword';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            name: 'contract_by_id',
            path: '/contract/id/:contractId',
            component: Contract,
        },
        {
            name: 'contract_add',
            path: '/contract/add',
            component: Contract,
        },
        {
            name: 'billing',
            path: '/billing',
            component: Billing,
        },
        {
            name: 'payments',
            path: '/payments',
            component: Payments,
        },
        {
            name: 'receipts',
            path: '/receipts',
            component: Receipts,
        },
        {
            name: 'cabinet',
            path: '/account',
            component: Account,
        },
        {
            name: 'forgot_password',
            path: '/password/reset',
            component: ForgotPassword,
        },
    ],
});