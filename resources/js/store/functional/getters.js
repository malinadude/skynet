let getters = {
    // Contract
    contracts: state => {
        return state.contracts;
    },
    contract: state => {
        return state.contract;
    },
    contractSettings: state => {
        return state.contractSettings;
    },
    contractHeadings: state => {
        return state.contractHeadings;
    },
    formErrors: state => {
        return state.formErrors;
    },

    // Payment
    payments: state => {
        return state.payments;
    },
    allPayments: state => {
        return state.allPayments;
    },
    importPaymentsTotal: state => {
        return state.importPaymentsTotal;
    },
    tableType: state => {
        return state.tableType;
    },

    // Billing
    columnsDisplay: state => {
        return state.columnsDisplay;
    },
    filtersDisplay: state => {
        return state.filtersDisplay;
    },

    // Receipts
    receipts: state => {
        return state.receipts;
    },

    // Sberbank Payment
    paymentGeneratedUrl: state => {
        return state.paymentGeneratedUrl;
    },
}

export default  getters