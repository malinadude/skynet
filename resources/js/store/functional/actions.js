import axios from 'axios';
import qs from 'query-string';

let actions = {
    // Contract
    createContract({commit}, contract) {
        commit('SET_FORM_ERRORS', []);

        return axios.post('/api/contract/add', contract)
        .then(res => {
            if (res.data.type === 'errors') {
                commit('SET_FORM_ERRORS', res.data.items);
            } else {
                commit('CREATE_CONTRACT', res.data.items);
            }
        }).catch(err => {
            return err;
        })
    },
    getContracts({commit}) {
        return axios.get('/api/contract/get')
        .then(res => {
            commit('GET_CONTRACTS', res.data);
        }).catch(err => {
            console.log(err);
        })
    },
    changeContract({commit}, contract) {
        commit('SET_FORM_ERRORS', []);

        return axios.post(`/api/contract/change`, contract)
        .then(res => {
            if (res.data.type === 'errors') {
                console.log('res.data.items', res.data.items)
                commit('SET_FORM_ERRORS', res.data.items);
            }
        }).catch(err => {
            console.log(err);
        })
    },
    getContract({commit}, contract_number) {
        return axios.get(`/api/contract/id/${contract_number}`)
        .then(res => {
            commit('GET_CONTRACT', res.data[0]);
        }).catch(err => {
            console.log(err);
        })
    },
    getContractSettings({commit}) {
        return axios.get(`/api/contract/settings`)
        .then(res => {
            commit('GET_CONTRACT_SETTINGS', res.data);
        }).catch(err => {
            console.log(err);
        })
    },
    getUserContract({commit}) {
        return axios.post(`/api/account/contract`)
        .then(res => {
            commit('GET_CONTRACT', res.data);
        }).catch(err => {
            console.log(err);
        })
    },
    setContractHeadings({commit}, contractHeadings) {
        commit('SET_CONTRACT_HEADINGS', contractHeadings);
    },

    // Payment
    getAllPayments({commit}) {
        return axios.get(`/api/payment/get`)
        .then(res => {
            commit('GET_ALL_PAYMENTS', res.data);
        }).catch(err => {
            console.log(err);
        })
    },
    getPayments({commit}, contract_number) {
        return axios.get(`/api/payment/get/${contract_number}`)
        .then(res => {
            commit('GET_PAYMENTS', res.data);
        }).catch(err => {
            console.log(err);
        })
    },
    getUserPayments({commit}) {
        return axios.get(`/api/payment/get/user`)
            .then(res => {
                commit('GET_PAYMENTS', res.data);
            }).catch(err => {
                console.log(err);
            })
    },
    replenishBalance({commit}, payment) {
        return axios.post('/api/payment/replenish', payment)
        .then(res => {
        }).catch(err => {
            return err;
        })
    },
    importPayments({commit}, payments) {
        return axios.post('/api/payment/import', payments)
        .then(res => {
            commit('SET_IMPORT_PAYMENTS_TOTAL', res.data);
        }).catch(err => {
            return err;
        })
    },
    // Billing
    setImportPaymentsTotal({commit}, importPaymentsTotal) {
        commit('SET_IMPORT_PAYMENTS_TOTAL', Object.assign({}, importPaymentsTotal));
    },
    paymentChange({commit}, payment) {
        return axios.post(`/api/payment/change`, payment)
        .then(res => {
        }).catch(err => {
            console.log(err);
        })
    },
    setTableType({commit}, type) {
        commit('SET_TABLE_TYPE', type);
    },

    // Billing
    columnsDisplay({commit}, column) {
        commit('SET_COLUMN_DISPLAY', column);
    },

    // Receipts
    receiptsGet({commit}) {
        return axios.get('/api/receipts/get')
        .then(res => {
            commit('GET_RECEIPTS', res.data);
        }).catch(err => {
            console.log(err);
        })
    },

    // Sberbank Payment
    paymentGenerateUrl({commit}, paymentData) {
        return axios.post(
            '/api/payment/generate',
            qs.stringify(paymentData),
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
        )
        .then(res => {
            commit('SET_PAYMENT_GENERATED_URL', res.data);
        }).catch(err => {
            return err;
        })
    },
}

export default actions