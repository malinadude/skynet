let mutations = {
    // Contract
    CREATE_CONTRACT(state, contract) {
        return state.contracts.unshift(contract);
    },
    GET_CONTRACTS(state, contracts) {
        return state.contracts = contracts;
    },
    GET_CONTRACT(state, contract) {
        return state.contract = contract;
    },
    GET_CONTRACT_SETTINGS(state, contractSettings) {
        return state.contractSettings = contractSettings;
    },
    SET_CONTRACT_HEADINGS(state, contractHeadings) {
        return state.contractHeadings = contractHeadings;
    },
    SET_FORM_ERRORS(state, errors) {
        return state.formErrors = errors;
    },

    // Payment
    GET_PAYMENTS(state, payments) {
        return state.payments = payments;
    },
    GET_ALL_PAYMENTS(state, allPayments) {
        return state.allPayments = allPayments;
    },
    REPLENISH_BALANCE(state, payment) {
        return state.payments.unshift(payment);
    },
    SET_IMPORT_PAYMENTS_TOTAL(state, importPaymentsTotal) {
        return state.importPaymentsTotal = importPaymentsTotal;
    },
    SET_TABLE_TYPE(state, type) {
        return state.tableType = type;
    },

    // Billing
    SET_COLUMN_DISPLAY(state, column) {
        return state.columnsDisplay.unshift(column);
    },

    // Billing
    GET_RECEIPTS(state, receipts) {
        return state.receipts.unshift(receipts);
    },

    // Sberbank Payment
    SET_PAYMENT_GENERATED_URL(state, url) {
        return state.paymentGeneratedUrl = url;
    },
}

export default mutations
