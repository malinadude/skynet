@extends('layouts.default')
@section('content')
    <div class="payments">
        <div class="container">
            <div class="wrapper">
                <div id="app">
                    <app></app>
                </div>
            </div>
        </div>
    </div>
@stop