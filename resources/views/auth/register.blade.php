@extends('layouts.default')

@section('content')
    <div class="auth auth_login">
        <div class="container">
            <div class="wrapper">
                <form class="form content-block" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="title__wrapper title_without-line">
                        <div class="title title_large">
                            Вход в личный кабинет
                        </div>
                    </div>

                    <div class="form__groups">
                        <div class="form__group">
                            <div class="form__fields">
                                <div class="form__field">
                                    <div class="title__wrapper">
                                        <div class="title title_small">Номер договора / Лицевой счет</div>
                                    </div>

                                    <input
                                    id="login"
                                    type="text"
                                    class="form-control @error('login') is-invalid @enderror"
                                    name="login"
                                    value="{{ old('login') }}"
                                    required
                                    autocomplete="login"
                                    autofocus
                                    >

                                    @error('number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form__field">
                                    <div class="title__wrapper">
                                        <div class="title title_small">Телефон</div>
                                    </div>

                                    <input
                                    id="phone"
                                    type="text"
                                    class="form-control @error('phone') is-invalid @enderror"
                                    name="phone"
                                    value="{{ old('phone') }}"
                                    required
                                    autocomplete="phone"
                                    autofocus
                                    >

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

{{--                                <div class="form__field">--}}
{{--                                    <div class="title__wrapper">--}}
{{--                                        <div class="title title_small">Email</div>--}}
{{--                                    </div>--}}

{{--                                    <input--}}
{{--                                    id="email"--}}
{{--                                    type="email"--}}
{{--                                    class="form-control @error('email') is-invalid @enderror"--}}
{{--                                    name="email"--}}
{{--                                    value="{{ old('email') }}"--}}
{{--                                    required--}}
{{--                                    autocomplete="email"--}}
{{--                                    >--}}

{{--                                    @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

                                <div class="form__field">
                                    <div class="title__wrapper">
                                        <div class="title title_small">Пароль</div>
                                    </div>

                                    <input
                                    id="password"
                                    type="password"
                                    class="form-control @error('password') is-invalid @enderror"
                                    name="password"
                                    required
                                    autocomplete="new-password"
                                    >

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form__field">
                                    <div class="title__wrapper">
                                        <div class="title title_small">Подтвердите пароль</div>
                                    </div>

                                    <input
                                    id="password-confirm"
                                    type="password"
                                    class="form-control"
                                    name="password_confirmation"
                                    required
                                    autocomplete="new-password"
                                    >
                                </div>
                            </div>

                            <div class="form__fields">
                                <div class="form__field">
                                    <button type="submit" class="button button_orange">
                                        Зарегистрироваться
                                    </button>
                                </div>
                                <div class="form__field">
                                    @if (Route::has('password.request'))
                                        <a class="button button_link" href="{{ route('password.request') }}">
                                            Забыли свой пароль?
                                        </a>
                                    @endif
                                </div>
                                <div class="form__field">
                                    <a class="button button_link" href="/login">
                                        Войти
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@endsection
