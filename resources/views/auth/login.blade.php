@extends('layouts.default')

@section('content')
    <div class="auth auth_login">
        <div class="container">
            <div class="wrapper">
                <form class="form content-block" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="title__wrapper title_without-line">
                        <div class="title title_large">
                            Вход в личный кабинет
                        </div>
                    </div>

                    <div class="form__groups">
                        <div class="form__group">
                            <div class="form__fields">
                                <div class="form__field">
                                    <div class="title__wrapper">
                                        <div class="title title_small">Номер договора / Лицевой счет</div>
                                    </div>

                                    <input
                                    id="login"
                                    type="text"
                                    class="form-control @error('login') is-invalid @enderror"
                                    name="login"
                                    value="{{ old('login') }}"
                                    required
                                    autocomplete="login"
                                    autofocus
                                    placeholder="Укажите номер договора / Л. С."
                                    >

                                    @error('login')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form__field">
                                    <div class="title__wrapper">
                                        <div class="title title_small">Пароль</div>
                                    </div>

                                    <input
                                    id="password"
                                    type="password"
                                    class="form-control @error('password') is-invalid @enderror"
                                    name="password"
                                    required
                                    autocomplete="current-password"
                                    placeholder="Введите пароль"
                                    >

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form__fields">
                                <div class="form__field">
                                    <button type="submit" class="button button_orange">
                                        Войти
                                    </button>
                                </div>
                                <div class="form__field">
                                    @if (Route::has('password.request'))
                                        <a class="button button_link" href="{{ route('password.request') }}">
                                            Получить пароль
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
