@extends('layouts.default')

@section('content')
    <div class="auth auth_reset">
        <div class="container">
            <div class="wrapper">
                <div id="app">
                    <app></app>
                </div>
            </div>
        </div>
    </div>
@endsection