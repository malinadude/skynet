<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
        <script>
            <?php if (Auth::check()) { ?>
                window.Laravel = <?php echo json_encode([
                    'api_token' => (Auth::user())->api_token,
                ]); ?>
            <?php } ?>
        </script>
    </head>
    <body>
        <header id="header" class="header">
            @include('includes.header')
        </header>

        <main id="main" class="main">
            @yield('content')
        </main>

        <footer id="footer" class="footer">
            @include('includes.footer')
        </footer>
    </body>
</html>