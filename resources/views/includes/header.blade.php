<?php
    $user = Auth::user();
?>

<div class="header">
    <div class="header__account">
        <div class="container">
            @if (!$user)
                <a id="account" class="link_hover" href="/">Личный кабинет</a>
            @else
                <a id="leave" class="link_hover" href="{{ url('/logout') }}">Выйти</a>
            @endif
        </div>
    </div>
    <div class="header__menu">
        <div class="container">
            <a href="/"><img src="/images/logo.svg" class="logo"></a>

            <div class="header__menu__listing">
                @if ($user && $user->role == 'admin' || $user && $user->role == 'manager')
                    <a href="/billing" class="header__menu__item link_hover">Биллинг</a>
                    <a href="/receipts" class="header__menu__item link_hover">Квитанции</a>
                    <a href="/payments" class="header__menu__item link_hover">Платежи</a>
                @else
                    <a href="#" class="header__menu__item link_hover">Интернет</a>
                    <a href="//i-skynet.ru/videonablyudenie" class="header__menu__item link_hover">Видеонаблюдение</a>
                    <a href="//i-skynet.ru/kontakty" class="header__menu__item link_hover">Контакты</a>
                    <a href="#" id="phone-callback" class="header__menu__item header__menu__item_blue link_hover">
                        Обратный звонок
                    </a>
                    <a href="tel:+7 928 117 79 78" class="header__menu__item link_hover">+7 928 117 79 78</a>
                @endif
            </div>
        </div>
    </div>
</div>