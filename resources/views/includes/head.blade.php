<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Skynet</title>

<link href="https://fonts.googleapis.com/css2?family=Rubik:wght@700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ mix('/css/styles.css') }}">
